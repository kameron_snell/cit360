import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;



public class Account {
    public String accountNumber;
    public String accountType;
    public String givenName;
    public String familyName;

    public Account(String accountNumber, String accountType, String givenName, String familyName) {
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public static void main(String[] args) {

        File file = new File("rawData.csv");

        TreeMap<String, Account> tmap = new TreeMap<>();

        try {
            // Read the text from the text file.
            String[] read = readText(file);

            // Print the text that was read from the text file.
            for (String line : read) {
                String[] array = line.split(",");
                if(!tmap.containsKey(array[0])){
                    Account temp = new Account(array[0], array[1],array[2],array[3]);
                    tmap.put(array[0],temp);
                }
            }
            for (Map.Entry<String,Account> entry : tmap.entrySet()) {
                System.out.println(entry.getValue().accountNumber + ", " + entry.getValue().accountType + ", " + entry.getValue().givenName + ", " + entry.getValue().familyName);

            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String[] readText(File file) throws IOException {

        ArrayList<String> text = new ArrayList<>();

        BufferedReader br = null;

        FileReader fr = new FileReader(file);

        try {
            br = new BufferedReader(fr);

            // Read each line from the file and add each line to the ArrayList.
            String line;
            while ((line = br.readLine()) != null) {
                text.add(line);
            }
        }
        finally {
            // Close the outermost reader that was successfully opened.
            if (br != null) {
                br.close();
            }
            else {
                fr.close();
            }
        }
        int size = text.size();
        String[] array = new String[size];
        return text.toArray(array);

    }


}



import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class MammalBeanTest {
    private static final double delta = 1e-6;

    @Test
    public void testMammal() {
        MammalBean m1 = new MammalBean(3, "red", 2.4);
        MammalBean m2 = new MammalBean(5, "black", 4.0);
        MammalBean m3 = new MammalBean(4, "white", 2.0);
        MammalBean m4 = new MammalBean(2, "blue", 3.0);

        assertEquals(3, m1.getLegCount());
        assertEquals("red", m1.getColor());
        assertEquals(2.4, m1.getHeight(), delta);

        assertEquals(5, m2.getLegCount());
        assertEquals("black", m2.getColor());
        assertEquals(4.0, m2.getHeight(), delta);

        assertEquals(4, m3.getLegCount());
        assertEquals("white", m3.getColor());
        assertEquals(2.0, m3.getHeight(), delta);

        assertEquals(2, m4.getLegCount());
        assertEquals("blue", m4.getColor());
        assertEquals(3.0, m4.getHeight(), delta);

    }

    @Test
    public void testDog(){
        DogBean d1 = new DogBean(4,"yellow",1.0,"terrier","George");
        DogBean d2 = new DogBean(3,"rainbow",2.0,"boxer","Coke");
        DogBean d3 = new DogBean(2,"magenta",3.0,"mutt","Hi");
        DogBean d4 = new DogBean(1,"grey",4.0,"wiener","Doxie");

        assertEquals(4,d1.getLegCount());
        assertEquals("yellow", d1.getColor());
        assertEquals(1.0,d1.getHeight(),delta);
        assertEquals("terrier",d1.getBreed());
        assertEquals("George",d1.getName());

        assertEquals(3,d2.getLegCount());
        assertEquals("rainbow", d2.getColor());
        assertEquals(2.0,d2.getHeight(),delta);
        assertEquals("boxer",d2.getBreed());
        assertEquals("Coke",d2.getName());

        assertEquals(2,d3.getLegCount());
        assertEquals("magenta", d3.getColor());
        assertEquals(3.0,d3.getHeight(),delta);
        assertEquals("mutt",d3.getBreed());
        assertEquals("Hi",d3.getName());

        assertEquals(1,d4.getLegCount());
        assertEquals("grey", d4.getColor());
        assertEquals(4.0,d4.getHeight(),delta);
        assertEquals("wiener",d4.getBreed());
        assertEquals("Doxie",d4.getName());
    }
    @Test
    public void setTest(){

        Set mammal = new HashSet();

        mammal.add("m1");
        mammal.add("m2");
        mammal.add("m3");
        mammal.add("m4");

        assertEquals(true, mammal.contains("m1"));
        assertTrue("m2", true);
        assertTrue("m3",true);
        assertEquals(false,mammal.contains("m5"));

        mammal.remove("m3");
        mammal.remove("m2");

        assertEquals(false,mammal.contains("m3"));
        assertEquals(false,mammal.contains("m2"));
        assertEquals(true,mammal.contains("m1"));
        assertEquals(true,mammal.contains("m4"));

       assertEquals(2,mammal.size());
    }

    @Test
    public void mapDog(){
        DogBean d1 = new DogBean(4,"yellow",1.0,"terrier","George");
        DogBean d2 = new DogBean(3,"rainbow",2.0,"boxer","Coke");
        DogBean d3 = new DogBean(2,"magenta",3.0,"mutt","Hi");
        DogBean d4 = new DogBean(1,"grey",4.0,"wiener","Doxie");

        HashMap<String, DogBean> hmap = new HashMap<>();

        hmap.put("George",d1);
        hmap.put("Coke",d2);
        hmap.put("Hi",d3);
        hmap.put("Doxie",d4);

        assertEquals(true,hmap.containsKey("George"));
        assertEquals(true,hmap.containsValue(d2));
        assertEquals(true,hmap.containsKey("Hi"));
        assertEquals(true,hmap.containsValue(d4));
        assertEquals(false,hmap.containsKey("doxie"));

        hmap.remove("George");
        hmap.remove("Hi");

        assertEquals(false,hmap.containsValue(d1));
        assertEquals(true,hmap.containsValue(d2));
        assertEquals(false,hmap.containsValue(d3));
        assertEquals(true,hmap.containsValue(d4));


    }







}

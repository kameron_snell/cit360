public class Kennel{


    public DogBean[] buildDogs() {

        DogBean[] dog = new DogBean[5];

        dog[0] = new DogBean(4,"blue", 12, "Shephard","Harry");
        dog[1] = new DogBean(3,"green",6, "Wiener","Billy");
        dog[2] = new DogBean(5,"black",19, "Tall", "Giant");
        dog[3] = new DogBean(4,"gold",7, "Retriever", "Fred");
        dog[4] = new DogBean(8, "white", 4, "Terrier", "Ed");

        return dog;
    }

    public void displayDogs (DogBean[] dog){
        for(int i = 0;i < dog.length;i++)
            System.out.println(dog[i].toString());
    }

    public static void main (String[] args){
        Kennel whatever = new Kennel();

        DogBean[] allDogs = whatever.buildDogs();

        whatever.displayDogs(allDogs);
    }
}
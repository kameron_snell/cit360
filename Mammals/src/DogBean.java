public class DogBean extends MammalBean {
    private String breed;
    private String name;

    public DogBean(int legCount, String color, double height, String breed, String name) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DogBean{"  + "leg Count='" + getLegCount() + '\'' +
                " Color='" + getColor() +'\'' +
                " Height='" + getHeight() + '\'' +
                " breed='" + breed + '\'' +
                ", name='" + name + '\'' +
                '}';
    }


}
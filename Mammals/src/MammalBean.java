public class MammalBean {

    private int legCount;
    private String color;
    private double height;

    public MammalBean(int legCount,String color,double height){
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }
}
